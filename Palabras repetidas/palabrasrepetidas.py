#!/usr/bin/python
# -*- coding: utf-8 -*-

print "*** Buscador de palabras dentro de un archivo ***"
print "*************************************************"
print ""

try:
	t1 = input("Inserte el primer texto o pulse enter para usar la fase por defecto (\"en un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía un hidalgo\"):")
except:
	t1 = "en un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía un hidalgo"

try:
	t2 = input("Ahora, el segundo texto o pulse enter para usar la frase por defecto (\"ponte en mi lugar, no tengo mucho tiempo para acordarme de tu nombre\")")
except:
	t2 = "ponte en mi lugar, no tengo mucho tiempo para acordarme de tu nombre"

repetidas = [] 
palabrasT1Limpias = []
palabrasT2Limpias = []

palabrasT1 = t1.split(' ')
palabrasT2 = t2.split(' ')

# Limpiamos las listas de palabras
for p in palabrasT1:
	p = p.replace(".","")
	p = p.replace(",","")
	p = p.replace(";","")
	p = p.replace("!","")
	p = p.replace("¡","")
	palabrasT1Limpias.append(p)

for p in palabrasT2:
	p = p.replace(".","")
	p = p.replace(",","")
	p = p.replace(";","")
	p = p.replace("!","")
	p = p.replace("¡","")
	palabrasT2Limpias.append(p)

# Comparamos
for p in palabrasT1Limpias:
	if p in palabrasT2Limpias and not(p in repetidas):
		repetidas.append(p)

print "Palabras repetidas: %s" % repetidas