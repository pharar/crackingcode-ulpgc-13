#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

print "*** Reconocimiento de Palíndromos ***"
print "*************************************"
print ""

print "Introduzca una cifra y comprobaremos si es un palíndromo: "

cifra =""
try:
	cifra = input("Cifra: ")
except:
	print "No ha introducido nada. Saliendo del programa!"
	sys.exit(1)

try:
	numero = int(cifra)
except:
	print "No has escrito un numero correcto. Saliendo del programa!"
	sys.exit(1)

if numero < 0:
	print "No se admiten números negativos, se toma el valor absoluto"
	numero = abs(numero)

variable = str(numero)
if variable == variable[::-1]:
	print "Es un palíndromo"
else:
	print "NO es un palíndromo"