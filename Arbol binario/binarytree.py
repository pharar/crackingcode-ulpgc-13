#!/usr/bin/python
# -*- coding: utf-8 -*-

print "*** Impresión de nodos hoja de un arbol binario ***"
print "***************************************************"
print ""

class Node:
	def __init__(self,info):
		self.info = info
		self.left = None
		self.right = None

	def __str__(self):
		return str(self.info)

class BinaryTree:

	def __init__(self,):
		self.root = None
		self.pointer = None

	def searchInBTree(self, root, valor):
		if root.info == valor:
			self.pointer = root
		elif valor > root.info:
			#print "Valor es %i > %i" % (valor,root.info)
			if root.right == None:
				#print "Devuelvo el nodo %i" % root.info
				self.pointer = root
			else:
				#print "Voy a la derecha del nodo %i" % root.info
				self.searchInBTree(root.right,valor)
		else:
			#print "Valor es %i < %i" % (valor,root.info)
			if root.left == None:
				#print "Devuelvo el nodo %i" % root.info
				self.pointer = root
			else:
				#print "Voy a la izquierda del nodo %i" % root.info
				self.searchInBTree(root.left, valor)


	def addingValue(self, valor):
		if self.root == None:
			self.root = Node(valor)
		else:
			#print "Vamos a buscar donde lo insertamos..."
			self.searchInBTree(self.root,valor)
			NodeAux = self.pointer
			if valor > NodeAux.info:
				#print "Añado el valor %i a la derecha de %i" % (valor,NodeAux.info)
				NodeAux.right = Node(valor)
			elif valor < NodeAux.info:
				#print "Añado el valor %i a la izquierda de %i" % (valor,NodeAux.info)
				NodeAux.left = Node(valor)


	def printLeaves(self, root):
		if root.left == None and root.right == None:
			return "Hoja: %i" % root.info
		else:
			#print "\t Estamos en el nodo %i" % (root.info)
			if root.left != None:
				#print "\t Hay elementos a la izquierda del nodo %i, vamos a ello..." % (root.info)
				res = self.printLeaves(root.left)
				if res != None: print "\t "+ res
			if root.right != None:
				#print "\t Hay elementos a la derecha del nodo %i, vamos a ello..." % (root.info)
				res=self.printLeaves(root.right)
				if res != None: print "\t "+ res

print """Se va a crear un árbol Binario.\n Añada todos los valores numéricos enteros que desee" 
 añadiendo el valor y pulsando la tecla enter.\n\n Nota: Cuando no quiera añadir más, no añada"
 valor y pulse enter.\n\n Nota2: Si no desea añadir valores y usar los que vienen por defecto "
 (8,3,1,6,4,7,10,14,13,5), pulse enter desde el principio."""

arbol = BinaryTree()
try:
	while 1:
		valor = input("Valor: ") 
		arbol.addingValue(valor)
except:
	if arbol.root == None:
		for valor in [8,3,1,6,4,7,10,14,13,5]:
			arbol.addingValue(valor)
print "Árbol creado!"

print "Nodos hoja: "
arbol.printLeaves(arbol.root)